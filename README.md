# Docker image for the [bot discord "Kassoubot"](https://gitlab.com/ConfrerieDuKassoulait/KassouBot) using Adoptium Eclipse Temurin latest 17

## Version **3.4**

### To use the docker image
Simply use this image: `registry.gitlab.com/confreriedukassoulait/lavalink:latest`

### To use locally
Run `download.bat` if you are on Windows or `download.sh` if on Debian-based distro.
It will download the Lavalink.jar server.
