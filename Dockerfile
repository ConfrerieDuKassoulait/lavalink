FROM eclipse-temurin:17-jdk

WORKDIR /opt

COPY download.sh .
RUN chmod u+x download.sh && ./download.sh && rm download.sh

COPY application.yml .

RUN groupadd -g 322 lavalink && \
    useradd -r -u 322 -g lavalink lavalink

USER lavalink

ENTRYPOINT ["java", "-Djdk.tls.client.protocols=TLSv1.3", "-Xmx3G", "-jar", "Lavalink.jar"]
